FROM httpd:2.4.63

# Installer les dépendances et le module mod_auth_openidc
RUN apt-get update && \
    apt-get upgrade -y --no-install-recommends && \
    apt-get install --no-install-recommends -y libapache2-mod-auth-openidc && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

RUN cp /usr/lib/apache2/modules/mod_auth_openidc.so /usr/local/apache2/modules/mod_auth_openidc.so

