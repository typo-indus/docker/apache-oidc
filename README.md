# 🐳 Image Docker Apache PHP pour Symfony

[![pipeline status](https://gitlab.com/typo-indus/docker/apache-oidc/badges/main/pipeline.svg)](https://gitlab.com/typo-indus/docker/apache-oidc/-/commits/main)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/52929168)](https://gitlab.com/typo-indus/docker/apache-oidc/-/commits/main)
[![Latest Release](https://gitlab.com/typo-indus/docker/apache-oidc/-/badges/release.svg)](https://gitlab.com/typo-indus/docker/apache-oidc/-/releases)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/typo-indus/docker/apache-oidc/-/blob/main/LICENSE)

Cette image Docker vous permet de lancer un proxy apache avec oauth-oidc

## 🚀 Utilisation

- Récupérez l'image Docker à partir du registre Docker :

```bash
docker pull registry.gitlab.com/typo-indus/docker/apache-oidc:<tag>
```

| tag        | description       |
|------------|-------------------|
| main       | Image principale. |

- Montez votre fichier de configuration à cet endroit : /usr/local/apache2/conf/httpd.conf
Voir un exemple de fichier : [exemple-httpd.conf](exemple-httpd.conf)

## 👥 Comment Contribuer

Pas de contribution pour le moment. Contactez-moi via les issues.

## 📄 License

Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.
