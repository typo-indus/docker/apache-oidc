.PHONY: help lint-yaml lint-markdown lint-dockerfile linter

.DEFAULT_GOAL := help

help: ## Afficher ce message d'aide
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)


lint-yaml: ## Lance le linter yaml
	@docker run --rm $$(tty -s && echo "-it" || echo) -v $(PWD):/data cytopia/yamllint:latest .

lint-markdown: ## Lance le linter markdown
	@docker run --rm -v $(PWD):/app -w /app registry.gitlab.com/typo-indus/docker/markdownlint:main /bin/sh -c \
		'markdownlint "**/*.md"'

lint-dockerfile: ## Lance le linter dockerfile
	@docker run --rm -v $(PWD):/app -w /app hadolint/hadolint:latest-debian hadolint Dockerfile

linter: lint-yaml lint-markdown lint-dockerfile ## Lance tous les linters
